/*
 * cuckoo hashing library
 *
 * (c) 2011 David Soria Parra
 */
#define _BSD_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "cuckoo.h"
#include "hash.h"

#define EPSILON 0.25
#define h1(t, k) ck_hash(t->size, t->h1, k)
#define h2(t, k) ck_hash(t->size, t->h2, k)

/* forward declaration */
static int ck_hash_rehash(ck_hash_t *t);

static
uint32_t ck_hash (uint32_t length, uint32_t r, const char *key) {
    uint32_t k;
    k = hash(key, length);

    return (k + r) % length;
}

static inline void ck_reinit_hashfunc(ck_hash_t *t)
{
    int f;
    f = open("/dev/urandom", O_RDONLY);
    read(f, &t->h1, sizeof(uint32_t));
    read(f, &t->h2, sizeof(uint32_t));
    close(f);
}

static inline int
ck_hash_new_bucket(ck_hash_t *t, ck_elm_t *e)
{
    uint32_t k, i;
    void *tmp;

    /* we abort if we use more than log(n) iterations. runtime is now
     * as bad as binary search (O(log n))*/
    for (i = 0; i < t->abort; i++) {
        k = h2(t, e->key);

        if (NULL == t->buckets[k]) {
            t->buckets[k] = e;
            return 0;
        }

        k = h1(t, e->key);
        if (NULL == t->buckets[k]) {
            t->buckets[k] = e;
            return 0;
        } else {
            tmp = t->buckets[k];
            t->buckets[k] = e;
            e = tmp;
        }
    }

    if (NULL == t->buckets[k]) {
        t->buckets[k] = e;
        return 0;
    }

    ck_hash_rehash(t);
    return ck_hash_new_bucket(t, e);
}

static int
ck_hash_rehash(ck_hash_t *t)
{
    int f;
    uint32_t i;
    ck_elm_t ** obuckets;

    /* new buckets */
    obuckets = t->buckets;

    t->buckets = (ck_elm_t **) malloc(t->size * sizeof(ck_elm_t *));

retry:
    memset(t->buckets, 0, t->size * sizeof(ck_elm_t *));
    ck_reinit_hashfunc(t);
    /* reinitialize hash functions */

    for (i = 0; i < t->size; i++) {
        uint32_t k;
        ck_elm_t * old;

        old = obuckets[i];
        if (NULL == old) {
            continue;
        }

        k = h1(t, old->key);
        if (NULL == t->buckets[k]) {
            t->buckets[k] = obuckets[i];
        } else {
            k = h2(t, old->key);
            if (NULL == t->buckets[k]) {
                t->buckets[k] = obuckets[i];
            } else if (ck_hash_new_bucket(t, t->buckets[k]) < 0) {
                goto retry;
            }
        }
    }

    return 0;
}

void
ck_hash_init(ck_hash_t *t, uint32_t size)
{
    int f;

    memset(t, 0, sizeof(ck_hash_t));
    t->size  = (uint32_t) 2 * (1 + EPSILON) * size;
    t->abort = ceil(log(t->size));

    ck_reinit_hashfunc(t);

    t->buckets = (ck_elm_t **) malloc(t->size * sizeof(ck_elm_t *));
    memset(t->buckets, 0, t->size * sizeof(ck_elm_t *));

    if (!t->buckets) {
        perror("cannot allocate memory");
        exit(-1);
    }

}

int
ck_hash_insert(ck_hash_t *t, char * key, void * element)
{
    uint32_t k;
    ck_elm_t * e;

    e = (ck_elm_t *) malloc(sizeof(ck_elm_t));
    e->key = key; //strdup(key);
    e->elm = element;

    k = h1(t, key);
    assert(k < t->size);

    if (t->buckets[k]) {
        return ck_hash_new_bucket(t, e);
    } else {
        t->buckets[k] = e;
    }

    return 0;
}

void *
ck_hash_lookup(ck_hash_t *t, const char * key)
{
    uint32_t k;

    k = h1(t, key);

    assert(k < t->size);
    if (t->buckets[k] && strcmp(t->buckets[k]->key, key) == 0) {
        return t->buckets[k]->elm;
    }

    k = h2(t, key);
    assert(k < t->size);
    if (t->buckets[k] && strcmp(t->buckets[k]->key, key) == 0) {
        return t->buckets[k]->elm;
    }

    return NULL;
}

int
ck_hash_remove(ck_hash_t *t, const char * key)
{
    /* do not forget to free the bucket element */
}

void
ck_hash_destroy(ck_hash_t *t)
{
    for (t->size--; t->size >= 0; t->size--) {
        if (t->buckets[t->size]) {
            free(t->buckets[t->size]->key);
            free(t->buckets[t->size]);
        }
    }
    free(t->buckets);
}
