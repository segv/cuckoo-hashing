#include <stdlib.h>
#include <stdio.h>
#include "cuckoo.h"

int main(void) {
    int a[] = {5, 10, 23, 42};
    int * b;
    ck_hash_t t;
    ck_hash_init(&t, 3);
    ck_hash_insert(&t, "hello", (void*) &a[0]);
    ck_hash_insert(&t, "test", (void*) &a[1]);
    ck_hash_insert(&t, "foob", (void*) &a[2]);
    b = (int*) ck_hash_lookup(&t, "hello");
    printf("%d\n", *b);
    b = (int*) ck_hash_lookup(&t, "foob");
    printf("%d\n", *b);
    return 0;
}
