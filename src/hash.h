#ifndef HASH_H
#define HASH_H

uint32_t hash(const char *, uint32_t);

#endif /* end of include guard: HASH_H */
