#ifndef CUCKOO_H
#define CUCKOO_H
#include <stdint.h>

#define MAX_LOOP 100

typedef struct ck_elm {
    char * key;
    void * elm;
} ck_elm_t;

typedef struct ck_hash {
    ck_elm_t ** buckets;
    uint32_t size;
    uint32_t abort;
    uint32_t h1;
    uint32_t h2;
} ck_hash_t;

void ck_hash_init(ck_hash_t *, uint32_t);
int ck_hash_insert(ck_hash_t *, char * key, void * element);
void* ck_hash_lookup(ck_hash_t *, const char * key);
int ck_hash_remove(ck_hash_t *, const char * key);
void ck_hash_destroy(ck_hash_t *);

#endif /* end of include guard: CUCKOO_H */
